
from Classes import SomeClass
from Classes import SomeOtherClass

def main():
    # instantiate a class
    print("############# CLASS EXAMPLES #################")
    print("############# SomeClass #################")
    someClass = SomeClass(10)

    # print 
    print("SomeNumber: " + str(someClass.someNumber))
    print("StaticNumber: " + str(someClass.someStatic))

    print("############# SomeOtherClass #################")
    someOtherClass = SomeOtherClass()

    input = "Some object"
    output = someOtherClass.returnArgument(input)
    print(output)

    print("")
    
    input2 = "Some other object"
    output, output2 = someOtherClass.returnMultipleArgument(input, input2)
    print(output)
    print(output2)

if __name__ == "__main__":
    main()
