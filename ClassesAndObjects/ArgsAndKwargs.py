class ArgsExample():
    def __init__(self, *args):
        self.args = args

    def showConstructorArgs(self):
        for arg in self.args:
            print(arg)

    def showMethodArgs(self, *args):
        for arg in args:
            print(arg)

    # def __printArgs(self, args):
    #     for arg in args:
    #         print(arg)

class KwargsExample():
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def showConstructorKwargs(self):
        for key, value in self.kwargs.items(): 
            print ("%s == %s" %(key, value)) 

    def showMethodKwargs(self, **kwargs):
        for key, value in kwargs.items(): 
            print ("%s == %s" %(key, value)) 

    # def __printKwargs(self, kwargs):
    #     for key, value in kwargs.items(): 
    #         print ("%s == %s" %(key, value)) 
    
