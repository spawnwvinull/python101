from ArgsAndKwargs import ArgsExample
from ArgsAndKwargs import KwargsExample

def main():
    # instantiate a class
    print("############# ARGS EXAMPLE #################")
    print("")
    args = ArgsExample("hallo", "ik", "zit in de", "args", 1, 2, 3, 4)
    args.showConstructorArgs()

    print("")
    
    args = ArgsExample()
    args.showMethodArgs("hallo", "ik", "zit in de", "args", 1, 2, 3, 4)

    # instantiate a class
    print("############# KWARGS EXAMPLE #################")
    print("")
    kwargs = KwargsExample(first = "hallo", second = "ik", third ="zit in de", fourth = "args", fifth = 1, etc = 2)
    kwargs.showConstructorKwargs()

    print("")

    kwargs.showMethodKwargs(iets = "completely", different = "iets anders")

if __name__ == "__main__":
    main()