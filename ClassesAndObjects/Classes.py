class SomeClass():
    someStatic = 10
    def __init__(self, someNumber):
        self.someNumber = someNumber # public
        self.test = 3                # public
        self._test = 2               # protected
        self.__test = 1              # private

    # public methods
    def publicMethod(self):
        return self.someVariable + 1

    # protected methods
    def protectedMethod(self):
        return self._test + 1
    
    # private methods
    def privateMethod(self):
        return self.__test + 1

class SomeOtherClass():
    def __init__(self):
        
        pass
    
    def returnArgument(self, input):
        return input

    def returnMultipleArgument(self, input1, input2):
        return input1, input2