import os
import csv


class FileExamples():
    def __init__(self):
        pass

    def createNewFile(self, fileName):
        with open(fileName, mode="w+") as t:
            return t

    def openExistingFile(self, fileName):
        with open(fileName) as t:
            return t

    def writeToFile(self, fileName, someText):
        with open(fileName, mode="w+") as t:
            t.write(someText + "\n")

    def readFromFile(self, fileName):
        with open(fileName, mode="r") as t:
            lines = t.readlines()
            result = [l.strip("\n") for l in lines]
            return result

    def checkIfExists(self, fileName):
        if os.path.exists(fileName):
            return True
        else:
            return False

    def removeFile(self, fileName):
        if os.path.exists(fileName):
            os.remove(fileName)

    def appendToFile(self, fileName, someText):
        with open(fileName, mode="a+") as t:
            t.write(someText + "\n")

    def read_csvSeperated(self, fileName):
        lines = []
        with open(fileName, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                lines.append(row)

        return lines

    def write_csvSeperated(self, fileName, columns, content):
        with open(fileName, 'w') as f:
            writer = csv.DictWriter(f, columns)
            writer.writeheader()
            for data in content:
                writer.writerow(data)
