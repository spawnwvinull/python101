from models.Persoon import Persoon
from models.Huisdier import Huisdier

class Testmyfirsttest():
    def testdoeiets(self):
        #arrange
        p = Persoon("Piet", huisdier = 0)

        #act
        result = p.geefnaam()


        #assert
        assert("Piet" == result)

    def testwelkhuisdier(self):
        #arrang
        naam = "Bob"
        huisdiersoort = "kat"
        huisdier = Huisdier(huisdiersoort)
        p = Persoon(naam, huisdier)

        #act
        result = p.geefhuisdier()

        #assert
        assert(huisdiersoort == result)