from models.functions import checkbalance
from models.functions import deposit
from models.functions import withdraw
import sys




while True:
    
    print("type d for deposit, w for withdraw and b to check your balance.")
    keuze = input("Maak uw keuze: ")
    if keuze.lower() == "d":
        deposit()
    elif keuze.lower() == "w":
        withdraw()
    elif keuze.lower() == "b":
        checkbalance()
    elif keuze.lower() == "q" or keuze.lower() == "quit" or keuze.lower() =="exit":
        print("exiting")
        sys.exit()
    else:
        print("Er ging iets mis.")
