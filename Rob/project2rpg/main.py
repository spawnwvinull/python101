from Room import Room
staat = {1:0,2:0,3:0,4:0,5:0}
sleutel = 0
amulet = 0
fm = "Er ging iets mis." #foutmelding
mk = "Maak je keuze. "   

def kamer1():
    while True:
        if staat[1] == 0:
            print("Je staat voor de piramide. De deur zit muurvast.")
            print("Er steek een steen uit naast de deur.")
            keuze = input("Druk 5 om de steen terug te duwen. ")
            if keuze == "5":
                print("Je duwt de steen terug naar binnen en de grote deur gaat open.")
                staat[1]=1
                r1 = Room(0,1,0,0)
                r1.richtingen()
                keuze2 = input(mk)
                if keuze2 == "8":
                    print("Je gaat de piramide in.")
                    kamer2()
                else:
                    print(fm)
            else:
                print(fm)
        else:
            print("Je staat voor de piramide. De deur is open.")
            r1 = Room(0,1,0,0)
            r1.richtingen()
            keuze = input(mk)
            if keuze == "8":
                kamer2()
            else:
                print(fm)



def kamer2():
    zippo = 0
    while True:
        if staat[2]== 0 and zippo == 0:
            print("Je bent in een ruimte maar je ziet niks.")
            print("Druk 5 om je zippo aan te steken, en 2 om terug te gaan.")
            keuze = input(mk)
            if keuze == "2":
                print("Je gaat terug de piramide uit.")
                kamer1()
            elif keuze == "5":
                print("Je steekt je zippo aan en kan een fakkel zien.")
                zippo = zippo + 1
            else:
                print(fm)

        elif staat[2]== 0 and zippo == 1:
            print("Druk 5 om de fakkel aan te steken en 2 om je zippo weg te stoppen en terug te gaan.")
            keuze = input(mk)
            if keuze == "2":
                print("Je stopt je zippo weg en gaat de piramide uit.")
                kamer1()
                zippo = zippo - 1
            elif keuze == "5":
                print("Je steekt de fakkel aan en langzaam licht de donkere kamer op.")
                staat[2] = 1
                zippo = zippo - 1

            else:
                print(fm)
        elif staat[2]== 1:
            print("Door de fakkel zie je dat je links en rechts doorgangen hebt.")
            r2=Room(1,0,1,1)
            r2.richtingen()
            keuze = input(mk)
            if keuze == "4":
                kamer3()
            elif keuze == "6":
                kamer4()
            elif keuze == "2":
                kamer1()
            else:
                print(fm)

def kamer3():
    while True:
        if staat[3]== 0:
            print("Er staat een standbeeld in het midden van de ruimte en er is een deur boven.")
            print("Druk 5 om het standbeeld te bekijken, 8 om naar de deur te gaan en 6 om terug te gaan.")
            keuze = input(mk)
            if keuze == "5":
                print("Het standbeeld houdt een amulet vast.")
                keuze2 = input("Druk 5 om het amulet te pakken en 6 om terug te gaan. ")
                if keuze2 == "5":
                    print("Je pakt het amulet. Het standbeeld stort in.")
                    staat[3]=1
                    amulet = 1
                elif keuze2 == "6":
                    print("Je gaat terug door de gang waar je vandaan kwam.")
                    kamer2()
                else:
                    print(fm)
            elif keuze == "6":
                print("je gaat terug naar de vorige kamer.")
                kamer2()  
            elif keuze == "8":
                print("De deur is op slot.")
            else:
                print(fm)

        elif staat[3]== 1:
            print("Je staat naast het ingestorte standbeeld.")
            print("Druk 8 om naar de deur boven te gaan en 6 om terug te gaan.")
            keuze3 = input(mk)
            if keuze3 == "8":
                if sleutel == 0:
                    print("De deur is op slot en je hebt geen sleutel.")
                else:
                    print("Je gebruikt de sleutel en duwd de zware deur open.")
                    staat[3] = 2
            elif keuze3 == "6":
                print("Je loopt terug de gang in waar je vandaan kwam.")
                kamer2()
            else:
                print(fm)
        elif staat[3] == 2:
            print("Je staat naast het ingestorte standbeeld. De deur boven staat open.")
            r3 =Room(0,1,1,0)
            r3.richtingen()
            keuze4 = input(mk)
            if keuze4 == "8":
                print("Je gaat de deur door.")
                kamer5()
            elif keuze4 =="6":
                kamer2()
            else:
                print(fm)

            


               






def kamer4():
    pass
def kamer5():
    pass

kamer1()
