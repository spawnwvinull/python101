from models.Games import Games

class Testofhetwerkt():
    def test1(self):  #of de gemiddelde prijs klopt
        #arrange
        g = Games("Resident evil 0", "horror", "gamecube", 30, 15)

        #act
        result = g.gemiddeldeprijs()

        #assert
        assert ( 22.50   == result)

    def test2(self): # laat alleen gamecube spellen zien.
        #arrange
        g = Games("Resident evil 0", "horror", "gamecube", 30, 15)
        g1 = Games("Mario kart", "racing", "gamecube", 50, 80)
        g2 = Games("Mario land", "platform", "gameboy", 15, 30)

        lijst = (g, g1, g2)
        for x in lijst:
            if x.console == "gamecube":
                return x.titel

        #act
        result = "Resident evil 0", "Mario kart"

        #assert
        assert ( x.titel == result)