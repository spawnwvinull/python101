class Games:
    def __init__(self, titel, genre, console, nieuwprijs, gebruiktprijs):
        self.titel = titel
        self.genre = genre
        self.console = console
        self.nieuwprijs = nieuwprijs
        self.gebruiktprijs = gebruiktprijs

    def gemiddeldeprijs(self):
        return(self.nieuwprijs + self.gebruiktprijs) / 2
