
class Klant():
    def __init__(self, naam, achternaam, klantnummer, tegoed):
        self.naam = naam
        self.achternaam = achternaam
        self.klantnummer = klantnummer
        self.tegoed = tegoed
        
    def volledigenaam(self):
        return self.naam + " " + self.achternaam

    def kankopen(self, bedrag):
        return self.tegoed >= bedrag

