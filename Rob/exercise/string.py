MyString = "   Hello World        "

print(MyString)
print(MyString.upper())
print(MyString.strip())
print(MyString.center(50, "*"))
print(MyString.strip().center(21, "*"))
print(MyString.strip().center(50,"*").upper())
