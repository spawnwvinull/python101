# list is a collection of objects
# lists are ordered
# lists can have duplicates
# lists are mutable
l = []
l = ['apple', 'banana', 'orange', "pear", "grape", "apple"]
print(l)
print(l)
print(l)

# print the number of elements in the list
print(len(l))

# print the first item in the list
print(l[0])

# add an item to the list
l.append("kiwi")
print(l)

# change the first item in the list
l[0] = "raspberry"
print(l)



l.append()
# remove an item from the list
l.remove("kiwi")
print(l)

# loop te list 1
for item in l:
    print(item)

# loop the list 2
for i, item in enumerate(l):
    print(item, i)

# list comprehension
# [expression for item in list]
print([x for x in l])
print([x for x in l if "g" in x])
