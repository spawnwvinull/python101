# dictionary is a collection of key value pairs
# dictionaries can have duplicates
# dictionaries are mutable
l = {"apple": "fruit", "orange": "fruit", "lettuce": "vegetable"}
print(l)

print(l.items())  # prints keys and values
print(l.keys())  # prints keys
print(l.values())  # prints values

# print a value
print(l["apple"])

# print a value
print(l.get("apple"))

# print the number of elements in the dictionary
print(len(l))

# print the keys
print(l.keys())

# print the values
print(l.values())

# print the key value pairs
for k, v in l.items():
    print(k, v)

# add an item to the dictionary
l["kiwi"] = "fruit"
print(l)

# add duplicate item
l["kiwi"] = "fruit"
print(l)

# change an item in the dictionary
l["kiwi"] = "vegetable"
print(l)

# remove an item in the dictionary
del l["kiwi"]
print(l)

# loop the dictionary
for k, v in l.items():
    print(k, v)

# list comprehension
# [expression for item in list]
print([x for x in l])
print([x for x in l if "g" in x])

print([x for x in l.items()])
print([x for x in l.items() if "g" in x])

print([x for x in l.keys()])
print([x for x in l.keys() if "g" in x])

print([x for x in l.values()])
print([x for x in l.values() if "g" in x])
