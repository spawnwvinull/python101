# set is a collection of objects
# sets can NOT have duplicates
# sets are mutable
# sets are UNORDERED
l = set()
l = {}
l = {'apple', 'banana', 'orange', "pear", "grape", "apple"}
print(l)
print(l)
print(l)

# print the number of elements in the set
print(len(l))

# print the first item in the set (makes no sense)
try:
    print(l[0])
except:
    print("sets are unordered and therefore cannot be indexed")

# add an item to the set
l.add("kiwi")
print(l)

# change the first item in the set
try:
    l[0] = "raspberry"
except:
    print('set cannot be indexed')
print(l)

# remove an item from the list
l.remove("kiwi")
print(l)

# loop te set 1
for item in l:
    print(item)

# loop the set 2
for i, item in enumerate(l):
    print(item, i)

# list comprehension
# [expression for item in list]
print([x for x in l])
print([x for x in l if "g" in x])


# put a list and a tuple in a set
l = {["dog", "cat"], ("apple", "banana")}
print(l)
