# same as lists but immutable
l = ()
l = ('apple', 'banana', 'orange', "pear", "grape", "apple")
print(l)
print(l)
print(l)

# print the number of elements in the tuple
print(len(l))

# print the first item in the tuple
print(l[0])

# add an item to the tuple
try:
    l.append("kiwi")
except:
    print('cannot append')
print(l)
print(l)
print(l)

# change the first item in the tuple
try:
    l[0] = "raspberry"
except:
    print('cannot change')
print(l)

# remove an item from the tuple
try:
    l.remove("kiwi")
except:
    print("cannot remove")
print(l)

# loop te tuple 1
for item in l:
    print(item)

# loop the tuple 2
for i, item in enumerate(l):
    print(item, i)

# list comprehension
# [expression for item in list]
print([x for x in l])
print([x for x in l if "g" in x])


# put a list in a tuple
l = (["dog", "cat"], ("apple", "banana"))
print(l)
