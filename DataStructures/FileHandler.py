import os

class FileHandler():
    def __init__(self):
        pass

    def createFile(self, path, fileName):
        fp = os.path.join(path, fileName)
        with open(fp, "w"):
            pass
    
    def writeTo(self, path, fileName, content, append=False):
        fp = os.path.join(path, fileName)
        if (append):
            with open(fp, "a") as f:
                f.write(content + "\n")
        else:
            with open(fp, "w") as f:
                f.write(content + "\n")

    def read(self, path, fileName):
        fp = os.path.join(path, fileName)
        with open(fp, "r") as f:
            content = f.readlines()
        content = [f.strip(os.linesep) for f in content]
        return content

    def read_csvSeperated(self, fileName):
        lines = []
        with open(fileName, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                lines.append(row)

        return lines