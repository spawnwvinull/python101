# Python101 
Repo https://gitlab.com/spawnwvinull/python101
Python tutorials.

TOC
IPython
Classes and objects
    self
    __init__.py
    typechecking
    Constructor
    Fields/Properties/Encapsulation
    Methods
    Return types
    Import statement
     *args
    *kwargs
DataStructures
    list
    tuple
    sets
    dictionaries
    
Coding styles
Project structure
    Readme
    Main
    Tests

PyTest
Virtual env
PIP
FLASK
Database

Debugging
Python: Configure Tests
set breakpoint
step over F10
step into F11
continue F5

