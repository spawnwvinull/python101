from FileAccess.FileExamples import FileExamples
import os


class TestFileExamplesCsv():
    def test_read_csvSeperated(self):
        # arrange
        fileName = "./Tests/Output/read_example.csv"
        fileExamples = FileExamples()

        # act
        result = fileExamples.read_csvSeperated(fileName)

        # assert
        assert(len(result) == 4)
        for row in result:
            print(row)
            assert(row["Naam"])
            print(row["Naam"])
            print(row["Beroep"])
            assert(row["Beroep"])
            assert(int(row["Leeftijd"]) > 18)
            print(row["Leeftijd"])

    # def test_write_csvSeperated(self):
    #     # arrange
    #     fileName = "./Tests/Output/write_example.csv"
    #     columns = ["Name", "Profession", "Age"]
    #     content = [
    #         {"Name": "Jan", "Profession": "Carpenter", "Age": 45},
    #         {"Name": "Piet", "Profession": "Firefigter", "Age": 60},
    #         {"Name": "Sjaan", "Profession": "Secretary", "Age": 27},
    #         {"Name": "Olivia", "Profession": "Surgeon", "Age": 48}
    #     ]
    #     fileExamples = FileExamples()

    #     # act
    #     result = fileExamples.write_csvSeperated(fileName, columns, content)

    #     # assert
    #     result = fileExamples.read_csvSeperated(fileName)
    #     for row in result:
    #         print(row)
    #         print(row["Name"])
    #         print(row["Profession"])
    #         print(row["Age"])
