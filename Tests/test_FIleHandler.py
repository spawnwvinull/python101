import os
from DataStructures.FileHandler import FileHandler


class TestFileHandler():
    def test_create_file(self):
        # arrange
        path = "c:\\temp"
        fileName = "test.txt"
        fh = FileHandler()

        # act
        fh.createFile(path, fileName)

        # assert
        exists = os.path.exists(os.path.join(path, fileName))
        assert(exists == True)

        # cleanup   
        os.remove(os.path.join(path, fileName))

    def test_writeTo_file(self):
        # arrange
        path = "c:\\temp"
        fileName = "test.txt"
        fh = FileHandler()
        expectedContent = "Dit is een stukje tekst"

        # act
        fh.writeTo(path, fileName, expectedContent)

        # assert
        exists = os.path.exists(os.path.join(path, fileName))
        assert(exists == True)

        actualContent = fh.read(path, fileName)
        assert(actualContent[0] == expectedContent)

        # cleanup   
        os.remove(os.path.join(path, fileName))

    def test_append_text(self):
        # arrange
        path = "c:\\temp"
        fileName = "append.txt"
        if os.path.exists(os.path.join(path, fileName)):
            os.remove(os.path.join(path, fileName))
        fh = FileHandler()
        fh.createFile(path, fileName)
        initialContent = "Dit is een stukje tekst 1"
        fh.writeTo(path, fileName, initialContent, False)

        expectedContent = "Dit is een stukje tekst 2"

        # act
        fh.writeTo(path, fileName, expectedContent, True)

        # assert
        actualContent = fh.read(path, fileName)
        assert(len(actualContent) == 2)
        assert(actualContent[1] == expectedContent)

        # act
        actualContent = fh.read(path, fileName)
        assert(len(actualContent) == 2)
        assert(actualContent[0] == initialContent)

        # cleanup   
        os.remove(os.path.join(path, fileName))
