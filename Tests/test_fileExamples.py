from FileAccess.FileExamples import FileExamples
import os


class TestFileExamples():
    def test_createNewFile(self):
        # arrange
        fileExamples = FileExamples()
        filePath = "./Tests/Output"
        fileName = "TestFile.txt"
        fullPath = os.path.join(filePath, fileName)

        # act
        fileExamples.createNewFile(fullPath)

        # assert
        assert(os.path.exists(fullPath))

        # cleanup
        os.remove(fullPath)

    def test_openExistingFile(self):
        # arrange
        fileExamples = FileExamples()
        existingFile = "./Tests/Output/Readme.md"

        # act
        f = fileExamples.openExistingFile(existingFile)

        # assert
        assert(f is not None)

    def test_writeToFile(self):
        # arrange
        fileExamples = FileExamples()
        fileName = "./Tests/Output/testfile.txt"
        someText = "some text"
        fileExamples.writeToFile(fileName, someText)

        # act
        fileExamples.writeToFile(fileName, someText)

        # assert
        lines = fileExamples.readFromFile(fileName)
        assert(lines[0] == someText)

    def test_appendToFile(self):
        # arrange
        fileExamples = FileExamples()
        fileName = "./Tests/Output/testfile_write.txt"
        someText1 = "some text1"
        someText2 = "some text2"

        fileExamples.removeFile(fileName)
        fileExamples.writeToFile(fileName, someText1)

        # act
        fileExamples.appendToFile(fileName, someText2)

        # assert
        lines = fileExamples.readFromFile(fileName)
        assert(lines[0] == someText1)
        assert(lines[1] == someText2)
        assert(len(lines) == 2)

    def test_checkIfExists(self):
        # arrange
        fileExamples = FileExamples()
        existingFile = "./Tests/Output/Readme.md"

        # act
        result = fileExamples.checkIfExists(existingFile)

        # assert
        assert(result == True)
